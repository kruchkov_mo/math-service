package controllers

import javax.inject._

import com.kruchkov.mathservice.solver.matherrors.MathCompilationError
import com.kruchkov.mathservice.solver.mathlatex.MathLatex
import com.kruchkov.mathservice.solver.mathlexer.MathLexer
import com.kruchkov.mathservice.solver.mathparser.{MathAST, MathParser}
import com.kruchkov.mathservice.solver.mathplot.MathPlot
import com.kruchkov.mathservice.solver.mathsolver.AbstractEvaluate
import play.api.mvc._
import play.api.libs.json._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class MainController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def evaluate() = Action { request =>
    request.body.asJson.map { json =>
      (json \ "expression").asOpt[String].map { expression =>
        go(expression) match {
          case Left(e: MathCompilationError) =>
            Ok(Json.parse(s"""
              {
                "math_error" : "${e.msg}"
              }
              """))
          case Right(resultMathPackage) =>
            MathPlot(resultMathPackage._3._1, resultMathPackage._3._2) match {
              case Left(e: MathCompilationError) =>
                val map: Map[String, String] = Map("MathFunc" -> resultMathPackage._1, "MathResultFunc" -> resultMathPackage._2, "MathPlotError" -> e.msg)
                val obj = Json.toJson(map)
                Ok(obj)
              case Right(name) =>
                val map: Map[String, String] = Map("MathFunc" -> resultMathPackage._1, "MathResultFunc" -> resultMathPackage._2, "MathPlotFileName" -> name.getOrElse("null"))
                val obj = Json.toJson(map)
                Ok(obj)
            }
        }
      }.getOrElse {
        BadRequest("Missing parameter [expression]")
      }
    }.getOrElse {
      BadRequest("Expecting Json data")
    }
  }

  private def go(code: String): Either[MathCompilationError, (String, String, (MathAST, List[String]))] = {
    for {
      tokens <- MathLexer(code).right
      ast <- MathParser(tokens).right
      inputMathFunc <- MathLatex(ast).right
      resultMathAst <- AbstractEvaluate(ast).right
      resultMathFunc <- MathLatex(resultMathAst._1).right
    } yield (inputMathFunc, resultMathFunc, resultMathAst)
  }
}
