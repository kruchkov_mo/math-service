package controllers

import javax.inject._
import play.api.mvc._
import play.api.libs.json._

import java.awt.image.BufferedImage
import java.io.{ByteArrayOutputStream, File, IOException, OutputStream}
import javax.imageio.ImageIO

import com.kruchkov.mathservice.solver.mathplot.MathPlot

@Singleton
class ResourceController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def getResource(resourceName: String) = Action {
    try {
      val path = MathPlot.getImagesPath
      val file: File = new File(path + resourceName)

      val image: BufferedImage = ImageIO.read(file)
      val baos = new ByteArrayOutputStream()
      ImageIO.write(image, "png", baos)
      Ok(baos.toByteArray()).as("image/png")
    } catch  {
      case e: IOException => NotFound(e.getMessage)
      case _ : Throwable => NotFound("Unknown error")
    }
  }
}
