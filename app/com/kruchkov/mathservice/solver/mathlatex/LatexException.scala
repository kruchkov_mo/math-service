package com.kruchkov.mathservice.solver.mathlatex

class LatexException(msg: String) extends Exception(msg)