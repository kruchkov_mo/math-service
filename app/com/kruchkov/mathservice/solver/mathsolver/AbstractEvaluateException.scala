package com.kruchkov.mathservice.solver.mathsolver

class AbstractEvaluateException(msg: String) extends Exception(msg)