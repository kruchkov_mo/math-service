name := "derivative_service"
organization := "com.kruchkov"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test

scalacOptions ++= Seq("-Ypatmat-exhaust-depth", "off")


// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.kruchkov.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.kruchkov.binders._"
