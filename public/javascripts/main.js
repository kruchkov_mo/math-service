function submitInputData(expressionStr) {
    //var calcServiceAddress = "http://localhost:9000/";
    var calcServiceAddress = "https://derivative-service.herokuapp.com/";
    var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xhr = new XHR();
    xhr.open('POST', calcServiceAddress + 'evaluate', true);
    xhr.onload = function() {
		console.log(this.responseText);
        if (this.status == 200) {	
			var obj = JSON.parse(this.responseText);	
			console.log(obj);
        } else {
            console.log('Status: ' + this.status + '; Error: ' + this.responseText);
        }
    };
    xhr.onerror = function() {
        console.log('Status: ' + this.status + '; Error: ' + this.responseText);
    }
    xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify({expression: expressionStr}));
}